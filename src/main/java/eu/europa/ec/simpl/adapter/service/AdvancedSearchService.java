package eu.europa.ec.simpl.adapter.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.adapter.client.FederatedCatalogueClient;
import eu.europa.ec.simpl.adapter.request.SearchRequest;
import eu.europa.ec.simpl.adapter.utils.AdvancedSearchPropertiesParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AdvancedSearchService {

    public static final String NODE_PROPERTY_PRICE = "price";
    public static final String CYPHER_RETURN_STATEMENT = "RETURN";
    public static final String DOT_SEPARATOR_STATEMENT = ".";
    public static final String COMMA_SEPARATOR_STATEMENT = ",";
    private final FederatedCatalogueClient federatedCatalogueClient;

    public String getQueryResultAdvancedSearch(final String authToken, final String request) {
        final var parsedRequest = AdvancedSearchPropertiesParser.parseRequest(request);
        final var federatedCatalogueSearchRequest = SearchRequest.builder()
                .statement(produceAdvancedSearchQueryStatement(parsedRequest))
                .parameters(Map.of())
                .build().serializeToJson();

        log.debug("federatedCatalogueSearchRequest: " + federatedCatalogueSearchRequest);
        final var response = federatedCatalogueClient.executeQueryWithTotalCount(federatedCatalogueSearchRequest);
        log.debug("response from catalogue: " + response);

        ObjectMapper objectMapper = new ObjectMapper();
        RoleFilterService roleFilter = new RoleFilterService(objectMapper, federatedCatalogueClient);
        var filteredItems = roleFilter.applyRolesToItems(authToken, response);

        JSONObject result = new JSONObject();
        result.put("totalCount", filteredItems.size());
        result.put("items", new JSONArray(filteredItems));
        return result.toString();
    }

    private String produceAdvancedSearchQueryStatement(final List<AdvancedSearchPropertiesParser.SearchProperty> properties) {

        final AtomicInteger nodeNumber = new AtomicInteger(1);
        final var statement = new StringBuilder();
        properties.forEach(property ->
                statement.append(produceMatchStatement(nodeNumber, property.getPropertyName(), property.getPropertyValue(),
                        property.getNodeName()))
        );

        statement.append(" " + CYPHER_RETURN_STATEMENT);
        final AtomicInteger nodeNumberForReturn = new AtomicInteger(1);

        properties.forEach(property ->
                statement.append(produceReturnElement(nodeNumberForReturn))
        );

        return statement.toString();
    }

    private String produceMatchStatement(final AtomicInteger nodeNumber, final String nodeProperty,
                                         final String nodePropertyValue, final String nodeName) {
        //special case for numeric range values
        if (NODE_PROPERTY_PRICE.equals(nodeProperty)) {
            return buildMatchStatementForNumericalRangeValue(nodeNumber, nodeProperty, nodePropertyValue, nodeName);
        }

        return String.format(" MATCH (n%s:%s) where n%s" + DOT_SEPARATOR_STATEMENT + "`%s` =~ '(?i).*%s.*'", nodeNumber.get(), nodeName,
                nodeNumber.getAndIncrement(), nodeProperty, nodePropertyValue);
    }

    private String produceReturnElement(final AtomicInteger nodeNumber) {
        if (nodeNumber.get() == 1) {
            return String.format(" n%s" + DOT_SEPARATOR_STATEMENT + "claimsGraphUri", nodeNumber.getAndIncrement());
        }

        return String.format(", n%s" + DOT_SEPARATOR_STATEMENT + "claimsGraphUri", nodeNumber.getAndIncrement());
    }

    public String produceNeo4jStatementToFilterUriListByRole(final List<String> claimGraphUris) {
        // Create a comma-separated string from the list of URIs, with each URI wrapped in single quotes
        final var commaSeparatedUrisAsString = claimGraphUris.stream()
                .map(uri -> "'" + uri + "'")
                .collect(Collectors.joining(COMMA_SEPARATOR_STATEMENT));

        // Generate the Cypher query with the comma-separated URIs
        return String.format("WITH [%s] AS uriList "
                + "MATCH (n:GeneralServiceProperties) "
                + "WHERE ANY(uri IN n" + DOT_SEPARATOR_STATEMENT + "claimsGraphUri WHERE uri IN uriList) "
                + CYPHER_RETURN_STATEMENT + " n", commaSeparatedUrisAsString);
    }

    private String buildMatchStatementForNumericalRangeValue(final AtomicInteger nodeNumber, final String nodeProperty,
                                                             final String nodePropertyValue, final String nodeName) {

        final var isExactValue = nodePropertyValue.matches("\\d+");
        final var isBothOperatorsIncluded = !isExactValue && nodePropertyValue.contains(COMMA_SEPARATOR_STATEMENT);

        if (isExactValue) {
            return String.format(" MATCH (n%s:%s) where toFloat(n%s" + DOT_SEPARATOR_STATEMENT + "`%s`) = %s", nodeNumber.get(), nodeName,
                    nodeNumber.getAndIncrement(), nodeProperty, nodePropertyValue);
        }

        if (isBothOperatorsIncluded) {
            final var firstOperatorWithValue = nodePropertyValue.split(COMMA_SEPARATOR_STATEMENT)[0];
            final var secondOperatorWithValue = nodePropertyValue.split(COMMA_SEPARATOR_STATEMENT)[1];

            return " MATCH (n" + nodeNumber.get() + ":" + nodeName + ") where toFloat(n" + nodeNumber.get() + DOT_SEPARATOR_STATEMENT + "`"
                    + nodeProperty + "`)" + firstOperatorWithValue + " and " + "toFloat(n" + nodeNumber.getAndIncrement() + DOT_SEPARATOR_STATEMENT + "`"
                    + nodeProperty + "`)" + secondOperatorWithValue;
        }

        return String.format(" MATCH (n%s:%s) where toFloat(n%s" + DOT_SEPARATOR_STATEMENT + "`%s`) %s", nodeNumber.get(), nodeName,
                nodeNumber.getAndIncrement(), nodeProperty, nodePropertyValue);
    }

}
