package eu.europa.ec.simpl.adapter.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "servicecatalogueclient", url = "${fc.client}")
public interface FederatedCatalogueClient {

    @PostMapping(path = "/query?queryLanguage=OPENCYPHER&timeout=5&withTotalCount=true", consumes = "application/json")
    String executeQueryWithTotalCount(@RequestBody final String jsonQuery);

    @PostMapping(path = "/query?queryLanguage=OPENCYPHER&timeout=5&withTotalCount=false", consumes = "application/json")
    String executeQueryWithoutTotalCount(@RequestBody final String jsonQuery);
}
