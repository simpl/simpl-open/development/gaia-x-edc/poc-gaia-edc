package eu.europa.ec.simpl.adapter.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static eu.europa.ec.simpl.adapter.utils.AdvancedSearchPropertiesParser.parseRequest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class AdvancedSearchPropertiesParserTest {

    private static final String VALID_JSON_REQUEST = """
            {
              "simpl:DataProperties": {
                "@type": "simpl:DataProperties",
                "producedBy": "John",
                "format": "JSON"
              },
              "simpl:OfferingPrice": {
                "@type": "simpl:OfferingPrice",
                "simpl:price": {
                  "op1": ">=",
                  "value1": "29.99"
                }
              }
            }
            """;

    private static final String JSON_WITH_PRICE_WITH_TWO_OPERATORS = """
            {
              "simpl:OfferingPrice": {
                "@type": "simpl:OfferingPrice",
                "simpl:price": {
                  "op1": "<=",
                  "value1": "49.99",
                  "op2": ">=",
                  "value2": "29.99"
                }
              }
            }
            """;

    @Test
    void parseRequestWithValidJson() {
        final var searchProperties = parseRequest(VALID_JSON_REQUEST);

        assertThat(searchProperties).hasSize(3);

        final var producedByProperty = searchProperties.getFirst();
        assertThat(producedByProperty.getPropertyName()).isEqualTo("producedBy");
        assertThat(producedByProperty.getPropertyValue()).isEqualTo("John");
        assertThat(producedByProperty.getNodeName()).isEqualTo("DataProperties");

        final var formatProperty = searchProperties.get(1);
        assertThat(formatProperty.getPropertyName()).isEqualTo("format");
        assertThat(formatProperty.getPropertyValue()).isEqualTo("JSON");
        assertThat(formatProperty.getNodeName()).isEqualTo("DataProperties");

        final var priceProperty = searchProperties.get(2);
        assertThat(priceProperty.getPropertyName()).isEqualTo("simpl:price");
        assertThat(priceProperty.getPropertyValue()).isEqualTo(">=29.99");
        assertThat(priceProperty.getNodeName()).isEqualTo("OfferingPrice");
    }

    @Test
    void parseRequestWithPriceRangeWithTwoOperators() {
        final var searchProperties = parseRequest(JSON_WITH_PRICE_WITH_TWO_OPERATORS);

        assertThat(searchProperties).hasSize(1);

        final var priceProperty = searchProperties.get(0);
        assertThat(priceProperty.getPropertyName()).isEqualTo("simpl:price");
        assertThat(priceProperty.getPropertyValue()).isEqualTo("<=49.99,>=29.99");
        assertThat(priceProperty.getNodeName()).isEqualTo("OfferingPrice");
    }

    @Test
    void parseRequestWithPriceWithoutOperators() {
        final var jsonWithOnlyOp1 = """
            {
              "simpl:OfferingPrice": {
                "@type": "simpl:OfferingPrice",
                "simpl:price": "20"
              }
            }
            """;

        final var searchProperties = parseRequest(jsonWithOnlyOp1);

        // Assert
        assertThat(searchProperties).hasSize(1);

        // Verify price range property
        final var priceProperty = searchProperties.get(0);
        assertThat(priceProperty.getPropertyName()).isEqualTo("simpl:price");
        assertThat(priceProperty.getPropertyValue()).isEqualTo("20");
        assertThat(priceProperty.getNodeName()).isEqualTo("OfferingPrice");
    }

    @ParameterizedTest
    @ValueSource(strings = {"hello there", "some+mail@yahoo.com", "https://lala-dubidu"})
    void parseRequestWithSpecialSignsShouldNotThrowExceptionWhenUsingAllowedSigns(final String value) {
        final var jsonWithAllowedChars = "          {\n"
                + "              \"simpl:OfferingPrice\": {\n"
                + "                \"@type\": \"simpl:OfferingPrice\",\n"
                + "                \"simpl:price\": \"" + value + "\"\n"
                + "              }\n"
                + "            }";

        assertThatCode(() -> parseRequest(jsonWithAllowedChars))
                .doesNotThrowAnyException();
    }

    @ParameterizedTest
    @ValueSource(strings = {"'<inject here>'", "&%$#@"})
    void parseRequestWithSpecialSignsShouldThrowExceptionWhenUsingNotAllowedSigns(final String value) {
        final var jsonWithAllowedChars = "          {\n"
                + "              \"simpl:OfferingPrice\": {\n"
                + "                \"@type\": \"simpl:OfferingPrice\",\n"
                + "                \"simpl:price\": \"" + value + "\"\n"
                + "              }\n"
                + "            }";

        assertThatThrownBy(() -> parseRequest(jsonWithAllowedChars))
                .isInstanceOf(IllegalArgumentException.class);
    }
}

