package eu.europa.ec.simpl.adapter.controller;

import eu.europa.ec.simpl.adapter.service.AdvancedSearchService;
import eu.europa.ec.simpl.adapter.service.FederatedCatalogueService;
import feign.FeignException.Unauthorized;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = FederatedCatalogueProxyController.class)
class FederatedCatalogueProxyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private FederatedCatalogueService federatedCatalogueService;

    @MockitoBean
    private AdvancedSearchService advancedSearchService;

    @Test
    void testQueryCatalogueQuickSearchShouldReturnSuccessResponse() throws Exception {
        String authToken = "Bearer testToken";
        String searchString = "param1,param2";
        String jsonResponse = "{\n" +
                "  \"totalCount\": 1,\n" +
                "  \"items\": [\n" +
                "    {\n" +
                "      \"i\": {\n" +
                "        \"identifier\": \"sdfsfd\",\n" +
                "        \"claimsGraphUri\": [\n" +
                "          \"did:web:registry.gaia-x.eu:DataOffering:MbPG4WfaAHa-uTXTEjj7SkeWb9j5-wHzvCjL\"\n" +
                "        ],\n" +
                "        \"description\": \"sdfsfd\",\n" +
                "        \"location\": \"sdf\",\n" +
                "        \"title\": \"sdf\"\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        when(federatedCatalogueService.getQueryResultQuickSearch(anyString(), anyString())).thenReturn(jsonResponse);

        final var requestBuilder = MockMvcRequestBuilders
                .post("/api/v1/search/quick")
                .header("Authorization", authToken)
                .param("searchString", searchString)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform((requestBuilder))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(jsonResponse));

        verify(federatedCatalogueService, times(1)).getQueryResultQuickSearch(authToken, searchString);
    }

    @Test
    void testQueryCatalogueQuickSearchCatalogTokenInvalid() throws Exception {
        String authToken = "Bearer testToken";
        String searchString = "param1,param2";

        when(federatedCatalogueService.getQueryResultQuickSearch(anyString(), anyString())).thenThrow(Unauthorized.class);

        final var requestBuilder = MockMvcRequestBuilders
                .post("/api/v1/search/quick")
                .header("Authorization", authToken)
                .param("searchString", searchString)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform((requestBuilder))
                .andExpect(status().isUnauthorized());

        verify(federatedCatalogueService, times(1)).getQueryResultQuickSearch(authToken, searchString);
    }

    @Test
    void queryCatalogueAdvancedSearchShouldReturnBodyWithStatus200() throws Exception {
        final var requestBuilder = MockMvcRequestBuilders
                .post("/api/v1/search/advanced")
                .header("Authorization", "authToken")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"mike\"}");

        when(advancedSearchService.getQueryResultAdvancedSearch(anyString(), anyString()))
                .thenReturn("response");

        mockMvc.perform((requestBuilder))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("response"));
    }
}