package eu.europa.ec.simpl.adapter.query;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class QuickSearchQueryGeneratorTest {

    @InjectMocks
    private QuickSearchQueryGenerator quickSearchQueryGenerator;

    @Test
    void shouldGenerateQueryWithValidTerms() {
        final String[] terms = {"term1", "term2"};
        final var result = quickSearchQueryGenerator.generateQuery(terms);

        assertThat(result)
                .isNotNull()
                .contains("value =~ '(?i).*term1.*'")
                .contains("value =~ '(?i).*term2.*'")
                .contains("CALL apoc.meta.nodeTypeProperties()");
    }
}