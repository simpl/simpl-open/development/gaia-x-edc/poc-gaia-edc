package eu.europa.ec.simpl.adapter.service;

import eu.europa.ec.simpl.adapter.client.FederatedCatalogueClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FederatedCatalogueServiceTest {

    @Mock
    private FederatedCatalogueClient federatedCatalogueClient;

    @InjectMocks
    private FederatedCatalogueService federatedCatalogueService;

    @Test
    void getQueryResultQuickSearchReturnValidResult() {
        String bearerToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIkNPTlNVTUVSX0NPREUiXX0."
            + "AFisZcuQYT09SlzEDqpR4-BMh09ziZt2jRnvVTVPeRM";
        String searchString = "param1,param2";
        String jsonResponse = "{\n" +
                "  \"totalCount\": 1,\n" +
                "  \"items\": [\n" +
                "    {\n" +
                "      \"i\": {\n" +
                "        \"identifier\": \"sdfsfd\",\n" +
                "        \"claimsGraphUri\": [\n" +
                "          \"did:web:registry.gaia-x.eu:DataOffering:MbPG4WfaAHa-uTXTEjj7SkeWb9j5-wHzvCjL\"\n" +
                "        ],\n" +
                "        \"description\": \"sdfsfd\",\n" +
                "        \"location\": \"sdf\",\n" +
                "        \"title\": \"sdf\"\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        
        String mockResponseForPoliciesQuery = """
                    {
                        "items": [
                            {
                                "n": {
                                    "claimsGraphUri": ["did:web:registry.gaia-x.eu:DataOffering:MbPG4WfaAHa-uTXTEjj7SkeWb9j5-wHzvCjL"]
                                },
                                "policy": {
                                    "access-policy": [
                                        "{\\"permission\\": [{\\"assignee\\": {\\"uid\\": \\"CONSUMER_CODE\\"}}]}"
                                    ]
                                }
                            }
                        ]
                    }
                    """;

        when(federatedCatalogueClient.executeQueryWithoutTotalCount(anyString()))
                .thenReturn(jsonResponse);
        
        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(mockResponseForPoliciesQuery);   // First call returns claimGraphUris

        String result = federatedCatalogueService.getQueryResultQuickSearch(bearerToken, searchString);

        assertNotNull(result);
        assertThat(result).contains("\"totalCount\":1");
        verify(federatedCatalogueClient, times(1)).executeQueryWithoutTotalCount(anyString());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", ","})
    void getQueryResultQuickSearchRaiseExceptionWhenParamsAreEmpty() {
        String searchString = null;

        assertThatThrownBy(() -> federatedCatalogueService.getQueryResultQuickSearch("dkdkdkd",searchString))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("Search string must not be empty");
    }

    @ParameterizedTest
    @ValueSource(strings = {"#", "@", "$", "%", "^", "&", "*", "(", ")", "!", "´", "p#", "p@", "p$", "p%", "p^", "p&", "p*", "p(", "p)", "p!", "p´", "a/a/"})
    void getQueryResultQuickSearchRaiseExceptionSearchstringContainsSpecialSymbols(final String searchString) {
        String bearerToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJlc2VhcmNoZXIiXX0."
            + "Dzod05tXTd52oub3vtW_j6sw2MsIBe8RGyq2Sdl_Gtc";
        assertThatThrownBy(() -> federatedCatalogueService.getQueryResultQuickSearch(bearerToken, searchString))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("Search string must not contain any special symbols");
    }
}